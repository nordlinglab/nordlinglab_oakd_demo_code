import cv2
import depthai as dai
import numpy as np

def createFaceDetectionPipeline(model_name, prev_w, prev_h):
    pipeline = dai.Pipeline()
    pipeline.setOpenVINOVersion(dai.OpenVINO.Version.VERSION_2021_2)

    cam_rgb = pipeline.createColorCamera()
    cam_rgb.setPreviewSize(prev_w,prev_h)
    cam_rgb.setInterleaved(False)
    xout_cam_rgb = pipeline.createXLinkOut()
    xout_cam_rgb.setStreamName('rgb')
    print('Created color camera')

    manip = pipeline.create(dai.node.ImageManip)
    manip.initialConfig.setResize(300,300)
    print('Created image manipulation node')

    monoLeft = pipeline.createMonoCamera()
    monoRight = pipeline.createMonoCamera()
    monoLeft.setBoardSocket(dai.CameraBoardSocket.LEFT)
    monoRight.setBoardSocket(dai.CameraBoardSocket.RIGHT)
    print('Created mono camera objects')

    depth = pipeline.createStereoDepth()
    depth.initialConfig.setConfidenceThreshold(200)

    nn = pipeline.createMobileNetSpatialDetectionNetwork()
    nn.setBlobPath(model_name)
    nn.setConfidenceThreshold(0.5)
    nn.setDepthLowerThreshold(100)
    nn.setDepthUpperThreshold(5000)
    xout_nn = pipeline.createXLinkOut()
    xout_nn.setStreamName('nn_out')
    print('Created face detection neural network')

    #Linking
    cam_rgb.preview.link(manip.inputImage)
    cam_rgb.preview.link(xout_cam_rgb.input)
    manip.out.link(nn.input)
    nn.out.link(xout_nn.input)
    monoLeft.out.link(depth.left)
    monoRight.out.link(depth.right)
    depth.depth.link(nn.inputDepth)
    print('Finished creating pipeline')
    return pipeline

model_name = 'face-detection-retail-0004_openvino_2021.2_8shave.blob'
frame_size = (500,500)
pipeline = createFaceDetectionPipeline(model_name, frame_size[0], frame_size[1])
device = dai.Device(pipeline)
device.startPipeline()

q_rgb = device.getOutputQueue(name = 'rgb', maxSize = 4, blocking = False)
q_nn = device.getOutputQueue(name = 'nn_out')

def calculate_distance(point1, point2):
    x1, y1, z1 = point1
    x2, y2, z2 = point2
    dx, dy, dz = x1 - x2, y1 - y2, z1 - z2
    return round(np.sqrt(dx ** 2 + dy ** 2 + dz ** 2),1)

while True:
    in_rgb = q_rgb.get()
    in_detection = q_nn.tryGet()

    rgb_frame = in_rgb.getCvFrame()

    if in_detection is not None:
        detections = in_detection.detections
        distance_results=[]
        for i, detection in enumerate(detections):
            bbox = (int(detection.xmin * frame_size[0]), int(detection.ymin * frame_size[1]), int(detection.xmax * frame_size[0]), int(detection.ymax * frame_size[1]))
            depth_x = detection.spatialCoordinates.x // 10 #rounded to the nearest cm
            depth_y = detection.spatialCoordinates.y // 10
            depth_z = detection.spatialCoordinates.z // 10
            cv2.rectangle(rgb_frame, (bbox[0], bbox[1]), (bbox[2], bbox[3]), (255, 0, 0), 2)
            cv2.putText(rgb_frame, 'x: '+str(depth_x),(bbox[0],bbox[1]+30), cv2.FONT_HERSHEY_TRIPLEX, 0.5, 255)
            cv2.putText(rgb_frame, 'y: '+str(depth_y),(bbox[0],bbox[1]+50), cv2.FONT_HERSHEY_TRIPLEX, 0.5, 255)
            cv2.putText(rgb_frame, 'z: '+str(depth_z),(bbox[0],bbox[1]+70), cv2.FONT_HERSHEY_TRIPLEX, 0.5, 255)

            for j, detection2 in enumerate(detections):
                if i<j:
                    point1 = depth_x, depth_y, depth_z
                    point2 = detection2.spatialCoordinates.x // 10, detection2.spatialCoordinates.y // 10, detection2.spatialCoordinates.z // 10,
                    distance = calculate_distance(point1, point2)
                    distance_results.append({'dist':distance, 'dangerous': distance < 100,
                        'detection1':detection, 'detection2':detection2})
        for result in distance_results:
            x1 = int(np.mean([result['detection1'].xmin, result['detection1'].xmax])*frame_size[0])
            y1 = int(result['detection1'].ymax*frame_size[1])
            x2 = int(np.mean([result['detection2'].xmin, result['detection2'].xmax])*frame_size[0])
            y2 = int(result['detection2'].ymax*frame_size[1])
            dist_x = int(np.mean([x1,x2]))
            dist_y = int(np.mean([y1,y2]))
            color = (0, 0, 255) if result['dangerous'] else (255, 0, 0)
            print(x1,x2,y1,y2)
            cv2.line(rgb_frame, (x1, y1), (x2, y2), color, 1)
            cv2.putText(rgb_frame, 'dist: '+str(result['dist'])+' cm', (dist_x,dist_y), cv2.FONT_HERSHEY_TRIPLEX, 0.5, color)
    cv2.imshow("rgb_preview", rgb_frame)
    if cv2.waitKey(1) == ord('q'):
        break
