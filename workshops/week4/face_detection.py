import cv2
import depthai as dai
import numpy as np

def createFaceDetectionPipeline(model_name, prev_w, prev_h):
    pipeline = dai.Pipeline()
    pipeline.setOpenVINOVersion(dai.OpenVINO.Version.VERSION_2021_2)

    cam_rgb = pipeline.createColorCamera()
    cam_rgb.setPreviewSize(prev_w,prev_h)
    cam_rgb.setInterleaved(False)
    xout_cam_rgb = pipeline.createXLinkOut()
    xout_cam_rgb.setStreamName('rgb')
    print('Created color camera')

    manip = pipeline.create(dai.node.ImageManip)
    manip.initialConfig.setResize(300,300)
    print('Created image manipulation node')

    nn = pipeline.createMobileNetDetectionNetwork()
    nn.setBlobPath(model_name)
    nn.setConfidenceThreshold(0.5)
    xout_nn = pipeline.createXLinkOut()
    xout_nn.setStreamName('nn_out')
    print('Created face detection neural network')

    #Linking
    cam_rgb.preview.link(manip.inputImage)
    cam_rgb.preview.link(xout_cam_rgb.input)
    manip.out.link(nn.input)
    nn.out.link(xout_nn.input)
    print('Finished creating pipeline')
    return pipeline

model_name = 'face-detection-retail-0004_openvino_2021.2_8shave.blob'
frame_size = (500,500)
pipeline = createFaceDetectionPipeline(model_name, frame_size[0], frame_size[1])
device = dai.Device(pipeline)
device.startPipeline()

q_rgb = device.getOutputQueue(name = 'rgb', maxSize = 4, blocking = False)
q_nn = device.getOutputQueue(name = 'nn_out')

while True:
    in_rgb = q_rgb.get()
    in_detection = q_nn.tryGet()

    rgb_frame = in_rgb.getCvFrame()

    if in_detection is not None:
        detections = in_detection.detections
        for detection in detections:
            bbox = bbox = (int(detection.xmin * frame_size[0]), int(detection.ymin * frame_size[1]), int(detection.xmax * frame_size[0]), int(detection.ymax * frame_size[1]))
            cv2.rectangle(rgb_frame, (bbox[0], bbox[1]), (bbox[2], bbox[3]), (0, 0, 255), 2)
    cv2.imshow("rgb_preview", rgb_frame)
    if cv2.waitKey(1) == ord('q'):
        break
