model_path = "human-pose-estimation-0001_openvino_2021.2_6shave.blob"
frame_size = (456, 256)
kp_conf_layer = 'Mconv7_stage2_L2'
kp_conf_shape = (1, 19, 32, 57)
paf_layer = 'Mconv7_stage2_L1'
paf_shape = (1, 38, 32, 57)
n_interp_samples, paf_score_th, conf_th = 10, 0.2, 0.4




