import cv2
import depthai as dai
import numpy as np
from Pose_info.plotting import pose_pairs, mapIdx, colors, pairs_num
from Pose_info.decode import model_path, frame_size, kp_conf_layer, kp_conf_shape, paf_layer, paf_shape

def createPoseEstimationPipeline(model_path, frame_size):
    pipeline = dai.Pipeline()
    cam_rgb = pipeline.createColorCamera()
    cam_rgb.setPreviewSize(frame_size)
    cam_rgb.setInterleaved(False)
    xout_cam_rgb = pipeline.createXLinkOut()
    xout_cam_rgb.setStreamName('rgb')
    print('Created color camera')

    pose_nn = pipeline.createNeuralNetwork()
    pose_nn.setBlobPath(model_path)
    xout_pose_nn = pipeline.createXLinkOut()
    xout_pose_nn.setStreamName('pose_nn')
    print('Created pose nn object')

    #Linking
    cam_rgb.preview.link(pose_nn.input)
    cam_rgb.preview.link(xout_cam_rgb.input)
    pose_nn.out.link(xout_pose_nn.input)
    print('Finished creating pipeline')
    return pipeline

pipeline = createPoseEstimationPipeline(model_path, frame_size)
device = dai.Device(pipeline)
device.startPipeline()

q_rgb = device.getOutputQueue(name = 'rgb', maxSize = 1, blocking = False)
q_pose_detections = device.getOutputQueue(name = 'pose_nn', maxSize = 1, blocking = False)

nPoints = 18
while True:
    in_rgb = q_rgb.get()
    in_pose_detections = q_pose_detections.tryGet()

    rgb_frame = in_rgb.getCvFrame()
    if in_pose_detections is not None:
        conf_map = np.array(in_pose_detections.getLayerFp16(kp_conf_layer)).reshape(kp_conf_shape)
        pafs = np.array(in_pose_detections.getLayerFp16(paf_layer)).reshape(paf_shape)
        maskedProbMap_list = []
        for i in range(nPoints):
            probMap = conf_map[0,i,:,:]
            probMap = cv2.resize(probMap, frame_size)
            mapMask = np.uint8(probMap>0.2)
            maskedProbMap = mapMask * probMap
            maskedProbMap_list.append(maskedProbMap)

        pafDisp_list=[]
        for i in range(len(mapIdx)):
            pafX = pafs[0, mapIdx[i][0]-pairs_num,:,:]
            pafY = pafs[0, mapIdx[i][1]-pairs_num,:,:]
            pafX = cv2.resize(pafX, frame_size)
            pafY = cv2.resize(pafY, frame_size)
            pafDisp = np.zeros((frame_size[1],frame_size[0]))
            print(pafX.shape)
            scale = 50
            for j in range(1,10):
                for k in range(1,10):
                    x1 = int(frame_size[0]/10*(j))
                    y1 = int(frame_size[1]/10*(k))
                    x2 = int(x1 + pafX[y1,x1]*scale)
                    y2 = int(y1 + pafY[y1,x1]*scale)
                    cv2.arrowedLine(pafDisp, (x1,y1), (x2, y2), 255, 2, cv2.LINE_AA)
            pafDisp_list.append(pafDisp)
        cv2.imshow('conf_map', maskedProbMap_list[2])
        cv2.imshow('paf', pafDisp_list[2])
    cv2.imshow('rgb_frame',rgb_frame)
    if cv2.waitKey(1)==ord('q'):
        break
