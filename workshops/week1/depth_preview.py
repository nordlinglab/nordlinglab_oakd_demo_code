import depthai as dai
import numpy as np
import cv2

def createDepthPipeline():
    pipeline = dai.Pipeline()
    cam_rgb = pipeline.createColorCamera()
    cam_rgb.setPreviewSize(640,400)
    xout_rgb = pipeline.createXLinkOut()
    xout_rgb.setStreamName('rgb')
    print('Created color camera object')

    monoLeft = pipeline.createMonoCamera()
    monoRight = pipeline.createMonoCamera()
    monoLeft.setBoardSocket(dai.CameraBoardSocket.LEFT)
    monoRight.setBoardSocket(dai.CameraBoardSocket.RIGHT)
    monoLeft.setResolution(dai.MonoCameraProperties.SensorResolution.THE_400_P)
    monoRight.setResolution(dai.MonoCameraProperties.SensorResolution.THE_400_P)
    xout_left = pipeline.createXLinkOut()
    xout_right = pipeline.createXLinkOut()
    xout_left.setStreamName('left')
    xout_right.setStreamName('right')
    print('Created mono camera objects')

    depth = pipeline.createStereoDepth()
    depth.setConfidenceThreshold(200)
    depth.setLeftRightCheck(True)
    #depth.setSubpixel(True)
    depth.setExtendedDisparity(True)
    xout_depth = pipeline.createXLinkOut()
    xout_depth.setStreamName('depth')
    print('Created depth object')

    #Linking
    monoLeft.out.link(depth.left)
    monoRight.out.link(depth.right)
    depth.disparity.link(xout_depth.input)

    monoLeft.out.link(xout_left.input)
    monoRight.out.link(xout_right.input)
    cam_rgb.preview.link(xout_rgb.input)
    print('Created pipeline succesfully')

    return pipeline

pipeline = createDepthPipeline()
device = dai.Device(pipeline)
device.startPipeline()

q_rgb = device.getOutputQueue(name = 'rgb', maxSize = 4, blocking = False)
q_left = device.getOutputQueue(name = 'left', maxSize = 4, blocking = False)
q_right = device.getOutputQueue(name = 'right', maxSize = 4, blocking = False)
q_depth = device.getOutputQueue(name = 'depth', maxSize = 4, blocking = False)

def min_max_norm(x):
    xmin = np.min(x)
    xmax = np.max(x)
    return (x-xmin)/(xmax-xmin)

while True:
    in_rgb = q_rgb.get()
    in_left = q_left.get()
    in_right = q_right.get()
    in_depth = q_depth.get()

    rgb_frame = in_rgb.getCvFrame()
    left_frame = in_left.getCvFrame()
    right_frame = in_right.getCvFrame()
    depth_map = in_depth.getCvFrame()
    depth_map = (min_max_norm(depth_map)*255).astype('uint8')

    cv2.imshow('rgb_preview', rgb_frame)
    cv2.imshow('left_preview', left_frame)
    cv2.imshow('right_preview', right_frame)
    cv2.imshow('depth_map', depth_map)

    if cv2.waitKey(1) == ord('q'):
        break
