import cv2
import depthai as dai
import numpy as np

def createDepthPipeline():
    pipeline = dai.Pipeline()
    cam_rgb = pipeline.createColorCamera()
    cam_rgb.setPreviewSize(640,400)
    cam_rgb.setColorOrder(dai.ColorCameraProperties.ColorOrder.RGB)
    xout_rgb = pipeline.createXLinkOut()
    xout_rgb.setStreamName('rgb')
    print('Created color camera')

    monoLeft = pipeline.createMonoCamera()
    monoRight = pipeline.createMonoCamera()
    monoLeft.setBoardSocket(dai.CameraBoardSocket.LEFT)
    monoRight.setBoardSocket(dai.CameraBoardSocket.RIGHT)
    monoLeft.setResolution(dai.MonoCameraProperties.SensorResolution.THE_400_P)
    monoRight.setResolution(dai.MonoCameraProperties.SensorResolution.THE_400_P)
    xout_left = pipeline.createXLinkOut()
    xout_right = pipeline.createXLinkOut()
    xout_left.setStreamName('left')
    xout_right.setStreamName('right')
    print('created mono camera objects')

    depth = pipeline.createStereoDepth()
    depth.setConfidenceThreshold(200)
    depth.setMedianFilter(dai.StereoDepthProperties.MedianFilter.MEDIAN_OFF)
    #depth.setLeftRightCheck(True) # Better handling for occlusions:
    #depth.setExtendedDisparity(True) # Better accuracy for shorter distance
    #depth.setSubpixel(True) # Better accuracy for longer distance
    xout_depth = pipeline.createXLinkOut()
    xout_depth.setStreamName('depth')
    print('created stereo depth object')

    #Linking
    monoLeft.out.link(depth.left)
    monoRight.out.link(depth.right)
    depth.disparity.link(xout_depth.input)

    monoLeft.out.link(xout_left.input)
    monoRight.out.link(xout_right.input)
    cam_rgb.preview.link(xout_rgb.input)
    print('pipeline created')
    return pipeline

pipeline = createDepthPipeline()
device = dai.Device(pipeline)
device.startPipeline()

q_rgb = device.getOutputQueue(name = 'rgb',maxSize =4,  blocking=False)
q_left = device.getOutputQueue(name = 'left',maxSize =4,  blocking=False)
q_right = device.getOutputQueue(name = 'right',maxSize =4,  blocking=False)
q_depth = device.getOutputQueue(name = 'depth',maxSize =4,  blocking=False)

def min_max_norm(x):
    xmin = np.min(x)
    xmax = np.max(x)
    return (x-xmin)/(xmax-xmin+0.0000001)

while True:
    in_rgb = q_rgb.get()
    in_left = q_left.get()
    in_right = q_right.get()
    in_depth = q_depth.get()

    rgb_frame = in_rgb.getCvFrame()
    left_frame = in_left.getCvFrame()
    right_frame = in_right.getCvFrame()
    depth_map = in_depth.getCvFrame()

    #Normalization of depth map
    depth_map = (min_max_norm(depth_map)*255).astype('uint8')

    #Display
    cv2.imshow('rgb_preview', rgb_frame)
    cv2.imshow('left_preview', left_frame)
    cv2.imshow('right_preview', right_frame)
    cv2.imshow('depth', depth_map)

    if cv2.waitKey(1) == ord('q'):
        break
