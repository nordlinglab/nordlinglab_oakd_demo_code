import numpy as np
import mediapipe_utils as mpu
import depthai as dai
import cv2

anchor_options = mpu.SSDAnchorOptions(num_layers=4,
                            min_scale=0.1484375,
                            max_scale=0.75,
                            input_size_height=128,
                            input_size_width=128,
                            anchor_offset_x=0.5,
                            anchor_offset_y=0.5,
                            strides=[8, 16, 16, 16],
                            aspect_ratios= [1.0],
                            reduce_boxes_in_lowest_layer=False,
                            interpolated_scale_aspect_ratio=1.0,
                            fixed_anchor_size=True)
anchors = mpu.generate_anchors(anchor_options)
nb_anchors = anchors.shape[0]
print('Finished creating '+str(nb_anchors)+' anchors')

def createHandLMPipeline(pd_score, pd_nms_thresh, pd_input_length, lm_input_length, video_size):
    pd_path = '../models/palm_detection.blob'
    lm_path = '../models/hand_landmark.blob'
    pipeline = dai.Pipeline()
    pipeline.setOpenVINOVersion(version = dai.OpenVINO.Version.VERSION_2021_2)

    print('Creating RGB camera')
    cam_rgb = pipeline.createColorCamera()
    cam_rgb.setPreviewSize(pd_input_length, pd_input_length)
    cam_rgb.setResolution(dai.ColorCameraProperties.SensorResolution.THE_1080_P)
    cam_rgb.setVideoSize(video_size,video_size)
    cam_rgb.setFps(30)
    cam_rgb.setInterleaved(False)
    xout_cam_rgb = pipeline.createXLinkOut()
    xout_cam_rgb.setStreamName('rgb')
    cam_rgb.video.link(xout_cam_rgb.input)

    print('Creating Palm detection model')
    pd_nn = pipeline.createNeuralNetwork()
    pd_nn.setBlobPath(pd_path)
    pd_nn.input.setQueueSize(1)
    pd_nn.input.setBlocking(False)
    cam_rgb.preview.link(pd_nn.input)
    xout_pd_nn = pipeline.createXLinkOut()
    xout_pd_nn.setStreamName('palm_detections')
    pd_nn.out.link(xout_pd_nn.input)

    print('Creating Hand Landmark detection model')
    lm_nn = pipeline.createNeuralNetwork()
    lm_nn.setBlobPath(lm_path)
    xin_lm_nn = pipeline.createXLinkIn()
    xin_lm_nn.setStreamName('lm_in')
    xin_lm_nn.out.link(lm_nn.input)
    xout_lm_nn = pipeline.createXLinkOut()
    xout_lm_nn.setStreamName('hand_landmark_preds')
    lm_nn.out.link(xout_lm_nn.input)

    print('Pipeline created')
    return pipeline
pd_score = 0.5
pd_nms_thresh = 0.3
video_size = 720
pd_input_length = 128
lm_input_length = 224
lm_score_threshold = 0.5
pipeline = createHandLMPipeline(pd_score, pd_nms_thresh, pd_input_length, lm_input_length, video_size)
device = dai.Device(pipeline)
device.startPipeline()

q_rgb = device.getOutputQueue(name='rgb', maxSize=4, blocking=False)
q_palm_detections = device.getOutputQueue(name='palm_detections')
q_lm_in = device.getInputQueue(name='lm_in')
q_hand_lm_preds = device.getOutputQueue(name='hand_landmark_preds')

def frame_norm(frame, bbox):
    print(np.array([*frame.shape[:2], *frame.shape[:2]])[::-1])
    return (np.array(bbox) * np.array([*frame.shape[:2], *frame.shape[:2]])[::-1]).astype(int)

def to_planar(arr: np.ndarray, shape: tuple):
    resized = cv2.resize(arr, shape)
    return resized.transpose(2,0,1)
rgb_frame = None
list_connections = [[0, 1, 2, 3, 4],
            [0, 5, 6, 7, 8],
            [5, 9, 10, 11, 12],
            [9, 13, 14 , 15, 16],
            [13, 17],
            [0, 17, 18, 19, 20]]

while True:
    in_rgb = q_rgb.tryGet()
    in_palm_detections = q_palm_detections.tryGet()
    in_hand_lm_preds = q_hand_lm_preds.tryGet()

    if in_rgb is not None:
        rgb_frame = in_rgb.getCvFrame()
    if in_palm_detections is not None:
        pd_inference = in_palm_detections
        scores = np.array(pd_inference.getLayerFp16("classificators"), dtype=np.float16) # 896
        bboxes = np.array(pd_inference.getLayerFp16("regressors"), dtype=np.float16).reshape((nb_anchors,18)) # 896x18
        # Decode bboxes
        regions = mpu.decode_bboxes(pd_score, scores, bboxes, anchors)
        # Non maximum suppression
        regions = mpu.non_max_suppression(regions, pd_nms_thresh)
        # transforms w,h, cx, cy to rectangle
        # also calculate the angle for having middle finge point up
        mpu.detections_to_rect(regions)
        #Expands and shifts the rectangle that contains the palm so that it's likely to cover the entire hand.
        mpu.rect_transformation(regions, video_size, video_size)

        for i,r in enumerate(regions):
            img_hand = mpu.warp_rect_img(r.rect_points, rgb_frame, lm_input_length, lm_input_length)
            cv2.imshow("hands", img_hand)
            nn_data = dai.NNData()
            nn_data.setLayer('input_1',to_planar(img_hand,(lm_input_length, lm_input_length)))
            #print('test...............................')
            q_lm_in.send(nn_data)
        # Retrieve landmarks
        for i,r in enumerate(regions):
            in_hand_lm_preds = q_hand_lm_preds.tryGet()
            if in_hand_lm_preds is not None:
                r.lm_score = in_hand_lm_preds.getLayerFp16('Identity_1')[0]
                r.handedness = in_hand_lm_preds.getLayerFp16('Identity_2')[0]
                lm_raw = np.array(in_hand_lm_preds.getLayerFp16("Identity_dense/BiasAdd/Add"))
                lm = []
                for i in range(int(len(lm_raw)/3)):
                    lm.append(lm_raw[3*i:3*(i+1)]/lm_input_length)
                r.landmarks = lm
                if rgb_frame is not None:
                    if r.lm_score > lm_score_threshold:
                        src = np.float32([[0, 0], [1, 0], [1, 1]])
                        dst = np.float32([ [x, y] for x,y in r.rect_points[1:]])
                        mat = cv2.getAffineTransform(src, dst)
                        lm_xy = np.expand_dims(np.array([(l[0], l[1]) for l in r.landmarks]), axis=0)
                        lm_xy = np.squeeze(cv2.transform(lm_xy, mat)).astype(np.int)
                        for point in lm_xy:
                            cv2.circle(rgb_frame, tuple(point), 10, (0, 0, 255),-1)
                        print(lm_xy.shape)
                        lines = [np.array([lm_xy[point] for point in line]) for line in list_connections]
                        cv2.polylines(rgb_frame, lines, False, (255, 0, 0), 2, cv2.LINE_AA)

                    cv2.imshow("rgb_preview", rgb_frame)
    if cv2.waitKey(1) == ord('q'):
        break
