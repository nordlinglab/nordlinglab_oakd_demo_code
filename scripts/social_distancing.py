import cv2
import depthai as dai
import numpy as np
import blobconverter
import uuid
import math

def calc_x(val, distance_bird_frame):
    norm = min(max_x, max(val, min_x))
    center = (norm - min_x) / (max_x - min_x) * distance_bird_frame.shape[1]
    bottom_x = max(center - 2, 0)
    top_x = min(center + 2, distance_bird_frame.shape[1])
    return int(bottom_x), int(top_x)

def calc_z(val, distance_bird_frame):
    norm = min(max_z, max(val, min_z))
    center = (1 - (norm - min_z) / (max_z - min_z)) * distance_bird_frame.shape[0]
    bottom_z = max(center - 2, 0)
    top_z = min(center + 2, distance_bird_frame.shape[0])
    return int(bottom_z), int(top_z)

def make_bird_frame():
    # fov = 68.7938
    fov = 100
    min_distance = 0.827
    frame = np.zeros((640, 400, 3), np.uint8)
    min_y = int((1 - (min_distance - min_z) / (max_z - min_z)) * frame.shape[0])
    cv2.rectangle(frame, (0, min_y), (frame.shape[1], frame.shape[0]), (70, 70, 70), -1)

    alpha = (180 - fov) / 2
    center = int(frame.shape[1] / 2)
    max_p = frame.shape[0] - int(math.tan(math.radians(alpha)) * center)
    fov_cnt = np.array([
        (0, frame.shape[0]),
        (frame.shape[1], frame.shape[0]),
        (frame.shape[1], max_p),
        (center, frame.shape[0]),
        (0, max_p),
        (0, frame.shape[0]),
    ])
    cv2.fillPoly(frame, [fov_cnt], color=(70, 70, 70))
    return frame

def createSocialDistancePipeline(model_name,prev_w,prev_h):
    pipeline = dai.Pipeline()
    pipeline.setOpenVINOVersion(dai.OpenVINO.Version.VERSION_2021_2)

    print('Creating RGB camera')
    cam_rgb = pipeline.createColorCamera()
    cam_rgb.setPreviewSize(prev_w,prev_h)
    cam_rgb.setResolution(dai.ColorCameraProperties.SensorResolution.THE_1080_P)
    cam_rgb.setInterleaved(False)
    xout_cam_rgb = pipeline.createXLinkOut()
    cam_rgb.preview.link(xout_cam_rgb.input)
    xout_cam_rgb.setStreamName('rgb')

    print('Creating depth perception')
    monoLeft = pipeline.createMonoCamera()
    monoRight = pipeline.createMonoCamera()
    monoLeft.setResolution(dai.MonoCameraProperties.SensorResolution.THE_400_P)
    monoRight.setResolution(dai.MonoCameraProperties.SensorResolution.THE_400_P)
    monoLeft.setBoardSocket(dai.CameraBoardSocket.LEFT)
    monoRight.setBoardSocket(dai.CameraBoardSocket.RIGHT)
    depth = pipeline.createStereoDepth()
    depth.setConfidenceThreshold(255)
    monoLeft.out.link(depth.left)
    monoRight.out.link(depth.right)

    print('Creating spatial detection NN')
    sd_nn = pipeline.createMobileNetSpatialDetectionNetwork()
    sd_nn.setBlobPath(str(blobconverter.from_zoo(name=model_name, shaves=6)))
    sd_nn.setConfidenceThreshold(0.5)
    sd_nn.input.setBlocking(False)
    sd_nn.setBoundingBoxScaleFactor(0.5)
    sd_nn.setDepthLowerThreshold(100)
    sd_nn.setDepthUpperThreshold(5000)
    xout_nn = pipeline.createXLinkOut()
    xout_nn.setStreamName('nn_out')
    cam_rgb.preview.link(sd_nn.input)
    sd_nn.out.link(xout_nn.input)
    depth.depth.link(sd_nn.inputDepth)
    print('Pipeline created')
    return pipeline

model_name='person-detection-retail-0013'
frame_size = (544,320)
max_z = 4
min_z = 1
max_x = 0.9
min_x = -0.7
pipeline =  createSocialDistancePipeline(model_name, frame_size[0], frame_size[1])

device = dai.Device(pipeline)
device.startPipeline()

q_rgb = device.getOutputQueue(name='rgb', maxSize=4, blocking=False)
q_detections = device.getOutputQueue(name='nn_out')

def calculate_distance(point1, point2):
    x1, y1, z1 = point1
    x2, y2, z2 = point2
    dx, dy, dz = x1 - x2, y1 - y2, z1 - z2
    distance = math.sqrt(dx ** 2 + dy ** 2 + dz ** 2)
    return distance

while True:
    in_rgb = q_rgb.tryGet()
    in_detections = q_detections.tryGet()
    if in_rgb is not None:
        rgb_frame = in_rgb.getCvFrame()
    if in_detections is not None:
        detections = in_detections.detections
        bboxes=[]
        # Decodes the detections
        for detection in detections:
            bboxes.append({
            'id': uuid.uuid4(),
            'label': detection.label,
            'confidence': detection.confidence,
            'x_min': int(detection.xmin * frame_size[0]),
            'x_max': int(detection.xmax * frame_size[0]),
            'y_min': int(detection.ymin * frame_size[1]),
            'y_max': int(detection.ymax * frame_size[1]),
            'depth_x': detection.spatialCoordinates.x / 1000,
            'depth_y': detection.spatialCoordinates.y / 1000,
            'depth_z': detection.spatialCoordinates.z / 1000,
            })
        # Draws bboes around the detected objects
        for bbox in bboxes:
            cv2.rectangle(rgb_frame, (bbox['x_min'], bbox['y_min']), (bbox['x_max'], bbox['y_max']), (0, 255, 0), 2)
            cv2.putText(rgb_frame, "x: {}".format(round(bbox['depth_x'], 1)), (bbox['x_min'], bbox['y_min'] + 30), cv2.FONT_HERSHEY_TRIPLEX, 0.5, 255)
            cv2.putText(rgb_frame, "y: {}".format(round(bbox['depth_y'], 1)), (bbox['x_min'], bbox['y_min'] + 50), cv2.FONT_HERSHEY_TRIPLEX, 0.5, 255)
            cv2.putText(rgb_frame, "z: {}".format(round(bbox['depth_z'], 1)), (bbox['x_min'], bbox['y_min'] + 70), cv2.FONT_HERSHEY_TRIPLEX, 0.5, 255)
            cv2.putText(rgb_frame, "conf: {}".format(round(bbox['confidence'], 1)), (bbox['x_min'], bbox['y_min'] + 90), cv2.FONT_HERSHEY_TRIPLEX, 0.5, 255)
            cv2.putText(rgb_frame, "label: {}".format(bbox['label'], 1), (bbox['x_min'], bbox['y_min'] + 110), cv2.FONT_HERSHEY_TRIPLEX, 0.5, 255)
        # Calculates distance and determine if detections are too close
        distance_results = []
        for i, detection1 in enumerate(bboxes):
            for detection2 in bboxes[i+1:]:
                point1 = detection1['depth_x'], detection1['depth_y'], detection1['depth_z']
                point2 = detection2['depth_x'], detection2['depth_y'], detection2['depth_z']
                distance = calculate_distance(point1, point2)
                #log.info("DG: {}".format(distance))
                distance_results.append({
                    'distance': distance,
                    'dangerous': distance < 1,
                    'detection1': detection1,
                    'detection2': detection2,
                })
        print(distance_results)
        # Create bird frame
        distance_bird_frame = make_bird_frame()
        too_close_ids = []
        bird_frame = distance_bird_frame.copy()
        # Draw the ellipse under predictions and lines to connect predictions
        for result in distance_results:
            x1 = result['detection1']['x_min'] + (result['detection1']['x_max'] - result['detection1']['x_min']) // 2
            y1 = result['detection1']['y_max']
            x2 = result['detection2']['x_min'] + (result['detection2']['x_max'] - result['detection2']['x_min']) // 2
            y2 = result['detection2']['y_max']
            color = (0, 0, 255) if result['dangerous'] else (255, 0, 0)
            cv2.ellipse(rgb_frame, (x1, y1), (40, 10), 0, 0, 360, color, thickness=cv2.FILLED)
            cv2.ellipse(rgb_frame, (x2, y2), (40, 10), 0, 0, 360, color, thickness=cv2.FILLED)
            cv2.line(rgb_frame, (x1, y1), (x2, y2), color, 1)
            label_size, baseline = cv2.getTextSize(str(round(result['distance'], 1)), cv2.FONT_HERSHEY_TRIPLEX, 0.5, 1)
            label_x = (x1 + x2 - label_size[0]) // 2
            label_y = (y1 + y2 - label_size[1]) // 2
            cv2.putText(rgb_frame, str(round(result['distance'], 1)), (label_x, label_y), cv2.FONT_HERSHEY_TRIPLEX, 0.5, color, 1)

        for result in distance_results:
            if result['dangerous']:
                left, right = calc_x(result['detection1']['depth_x'], bird_frame)
                top, bottom = calc_z(result['detection1']['depth_z'], bird_frame)
                cv2.rectangle(bird_frame, (left, top), (right, bottom), (0, 0, 255), 2)
                too_close_ids.append(result['detection1']['id'])
                left, right = calc_x(result['detection2']['depth_x'], bird_frame)
                top, bottom = calc_z(result['detection2']['depth_z'], bird_frame)
                cv2.rectangle(bird_frame, (left, top), (right, bottom), (0, 0, 255), 2)
                too_close_ids.append(result['detection2']['id'])
                print(too_close_ids)

        for result in bboxes:
            if result['id'] not in too_close_ids:
                left, right = calc_x(result['depth_x'], bird_frame)
                top, bottom = calc_z(result['depth_z'], bird_frame)
                cv2.rectangle(bird_frame, (left, top), (right, bottom), (0, 255, 0), 2)

        if rgb_frame is not None:
            cv2.imshow("rgb_preview", rgb_frame)
            cv2.imshow("Frame", bird_frame)
    if cv2.waitKey(1) == ord('q'):
        break
