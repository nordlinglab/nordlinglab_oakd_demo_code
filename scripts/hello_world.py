import numpy as np
import cv2
import depthai as dai

pipeline=dai.Pipeline()

cam_rgb = pipeline.createColorCamera()
cam_rgb.setPreviewSize(300,300)
cam_rgb.setInterleaved(False)

detection_nn = pipeline.createNeuralNetwork()
detection_nn.setBlobPath('../models/mobilenet-ssd.blob')

cam_rgb.preview.link(detection_nn.input)

xout_rgb = pipeline.createXLinkOut()
xout_rgb.setStreamName('rgb')
cam_rgb.preview.link(xout_rgb.input)

xout_nn = pipeline.createXLinkOut()
xout_nn.setStreamName('nn')
detection_nn.out.link(xout_nn.input)

device = dai.Device(pipeline)
device.startPipeline()

q_rgb = device.getOutputQueue('rgb')
q_nn = device.getOutputQueue('nn')

frame = None
bboxes = []

def frame_norm(frame, bbox):
    return (np.array(bbox) * np.array([*frame.shape[:2], *frame.shape[:2]])[::-1]).astype(int)

classes=['background','aeroplane','bicycle','bird','boat','bottle','bus','car',
'cat','chair','cow','diningtable','dog','horse','motorbike','person','pottedplant','sheep'
'sofa','train','tvmonitor']

while True:
    in_rgb = q_rgb.tryGet()
    in_nn = q_nn.tryGet()

    if in_rgb is not None:
        shape = (3, in_rgb.getHeight(), in_rgb.getWidth())
        frame = in_rgb.getData().reshape(shape).transpose(1,2,0).astype(np.uint8)
        frame = np.ascontiguousarray(frame)

    if in_nn is not None:
        bboxes = np.array(in_nn.getFirstLayerFp16())
        bboxes = bboxes[:np.where(bboxes==-1)[0][0]]
        bboxes = bboxes.reshape((bboxes.size//7,7))
        bboxes = bboxes[bboxes[:,2]>0.8][:,[1,3,4,5,6]]
        bboxes = bboxes[bboxes[:,0]<20]

    if frame is not None:
        for raw_bbox in bboxes:
            print(raw_bbox)
            bbox = frame_norm(frame,raw_bbox[1:5])
            label = classes[raw_bbox[0].astype(int)]
            print(label,bbox)
            cv2.rectangle(frame, (bbox[0],bbox[1]), (bbox[2], bbox[3]), (255,0,0), 2)
            cv2.putText(frame, label, (bbox[0],bbox[1]),cv2.FONT_HERSHEY_SIMPLEX,1,(0,0,0))
        cv2.imshow('preview',frame)

    if cv2.waitKey(1) == ord('q'):
        break
