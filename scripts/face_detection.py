import cv2
import depthai as dai
import numpy as np

def createFaceDetectionPipeline(blob_path, prev_size):
    pipeline=dai.Pipeline()
    print('Creating color camera')
    cam_rgb=pipeline.createColorCamera()
    xout_rgb=pipeline.createXLinkOut()
    xout_rgb.setStreamName('rgb')

    cam_rgb.setPreviewSize(prev_size)
    cam_rgb.setInterleaved(False)

    cam_rgb.preview.link(xout_rgb.input)
    print('Creating face detection NN')
    detection_nn = pipeline.createNeuralNetwork()
    detection_nn.setBlobPath(blob_path)
    xout_detection_nn = pipeline.createXLinkOut()
    xout_detection_nn.setStreamName('face_detections')
    detection_nn.out.link(xout_detection_nn.input)

    cam_rgb.preview.link(detection_nn.input)
    print('Pipeline created')
    return pipeline

blob_path = '../models/face-detection-retail-0004_openvino_2021.2_8shave.blob'
prev_size = (300,300)
pipeline = createFaceDetectionPipeline(blob_path, prev_size)
device = dai.Device(pipeline)
device.startPipeline()

q_rgb = device.getOutputQueue(name='rgb',  maxSize=4, blocking=False)
q_face_detections = device.getOutputQueue(name ='face_detections')

rgb_frame = None
bboxes = []

def frame_norm(frame, bbox):
    return (np.array(bbox) * np.array([*frame.shape[:2], *frame.shape[:2]])[::-1]).astype(int)

while True:
    in_rgb = q_rgb.tryGet()
    in_face_detections = q_face_detections.tryGet()

    if in_rgb is not None:
        rgb_frame = in_rgb.getCvFrame()

    if in_face_detections is not None:
        bboxes = np.array(in_face_detections.getFirstLayerFp16())
        bboxes = bboxes.reshape((bboxes.size//7,7))
        print(bboxes)
        bboxes = bboxes[bboxes[:,2]>0.7][:,3:7]

    if rgb_frame is not None:
        for raw_bbox in bboxes:
            print(raw_bbox)
            #print(raw_bbox)
            bbox = frame_norm(rgb_frame,raw_bbox)
            print(bbox)
            cv2.rectangle(rgb_frame, (bbox[0],bbox[1]), (bbox[2], bbox[3]), (255,0,0), 2)

        cv2.imshow("rgb_preview", rgb_frame)
    if cv2.waitKey(1) == ord('q'):
        break
