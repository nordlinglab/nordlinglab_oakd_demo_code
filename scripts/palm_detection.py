import numpy as np
import mediapipe_utils as mpu
import depthai as dai
import cv2

anchor_options = mpu.SSDAnchorOptions(num_layers=4,
                            min_scale=0.1484375,
                            max_scale=0.75,
                            input_size_height=128,
                            input_size_width=128,
                            anchor_offset_x=0.5,
                            anchor_offset_y=0.5,
                            strides=[8, 16, 16, 16],
                            aspect_ratios= [1.0],
                            reduce_boxes_in_lowest_layer=False,
                            interpolated_scale_aspect_ratio=1.0,
                            fixed_anchor_size=True)
anchors = mpu.generate_anchors(anchor_options)
nb_anchors = anchors.shape[0]
print('Finished creating '+str(nb_anchors)+' anchors')

def createPalmDetectionPipeline(pd_path, pd_score, pd_nms_thresh, pd_input_length, video_size):
    pipeline = dai.Pipeline()
    pipeline.setOpenVINOVersion(version = dai.OpenVINO.Version.VERSION_2021_2)

    print('Creating color camera for palm detection model')
    cam_rgb = pipeline.createColorCamera()
    cam_rgb.setPreviewSize(pd_input_length,pd_input_length)
    cam_rgb.setResolution(dai.ColorCameraProperties.SensorResolution.THE_1080_P)
    cam_rgb.setVideoSize(video_size, video_size)
    cam_rgb.setFps(30)
    cam_rgb.setInterleaved(False)
    xout_cam_rgb = pipeline.createXLinkOut()
    xout_cam_rgb.setStreamName('rgb')
    cam_rgb.video.link(xout_cam_rgb.input) # we need to link the video because preview is too small

    print('Creating Palm detection model')
    pd_nn = pipeline.createNeuralNetwork()
    pd_nn.setBlobPath(pd_path)
    pd_nn.input.setQueueSize(1)
    pd_nn.input.setBlocking(False)
    cam_rgb.preview.link(pd_nn.input)
    xout_pd_nn = pipeline.createXLinkOut()
    xout_pd_nn.setStreamName('palm_detections')
    pd_nn.out.link(xout_pd_nn.input)
    print('Pipeline created')
    return pipeline

pd_path = '../models/palm_detection.blob'; pd_input_length = 128 # Go together
pd_score = 0.5
pd_nms_thresh = 0.3
video_size = 720 # must be squre
pipeline = createPalmDetectionPipeline(pd_path, pd_score, pd_nms_thresh, pd_input_length, video_size)
device = dai.Device(pipeline)
device.startPipeline()

q_rgb = device.getOutputQueue(name='rgb',  maxSize=4, blocking=False)
q_palm_detections = device.getOutputQueue(name ='palm_detections')

rgb_frame = None
regions=[]
def frame_norm(frame, bbox):
    return (np.array(bbox) * np.array([*frame.shape[:2], *frame.shape[:2]])[::-1]).astype(int)

while True:
    in_rgb = q_rgb.tryGet()
    in_palm_detections = q_palm_detections.tryGet()
    if in_rgb is not None:
        rgb_frame = in_rgb.getCvFrame()
    if in_palm_detections is not None:
        #print('there is palm')

        inference = in_palm_detections
        scores = np.array(inference.getLayerFp16("classificators"), dtype=np.float16) # 896
        bboxes = np.array(inference.getLayerFp16("regressors"), dtype=np.float16).reshape((nb_anchors,18)) # 896x18
        # Decode bboxes
        regions = mpu.decode_bboxes(pd_score, scores, bboxes, anchors)
        # Non maximum suppression
        regions = mpu.non_max_suppression(regions, pd_nms_thresh)
    if rgb_frame is not None:
        for r in regions:
            box = frame_norm(rgb_frame, r.pd_box)
            cv2.rectangle(rgb_frame, (box[0], box[1]), (box[0]+box[2], box[1]+box[3]), (255,0,0), 2)
        cv2.imshow("rgb_preview", rgb_frame)
    if cv2.waitKey(1) == ord('q'):
        break
