import cv2
import depthai as dai
import numpy as np
import Pose_info

blob_path = "../models/human-pose-estimation-0001_openvino_2021.2_6shave.blob"
prev_size = (456, 256)

def create_pipeline():
    pipeline = dai.Pipeline()
    cam_rgb = pipeline.createColorCamera()
    cam_rgb.setPreviewSize(prev_size)    
    cam_rgb.setInterleaved(False)
    xout_rgb = pipeline.createXLinkOut()
    xout_rgb.setStreamName("rgb")
    cam_rgb.preview.link(xout_rgb.input)
    pose_nn = pipeline.createNeuralNetwork()
    pose_nn.setBlobPath(blob_path)
    pose_nn_xout = pipeline.createXLinkOut()
    pose_nn_xout.setStreamName("pose_nn")
    pose_nn.out.link(pose_nn_xout.input)
    cam_rgb.preview.link(pose_nn.input)
    return pipeline

pipeline = create_pipeline()
device = dai.Device(pipeline)
device.startPipeline()
q_rgb = device.getOutputQueue("rgb", 1, True)
pose_nn = device.getOutputQueue("pose_nn", 1, False)

running, pose, keypoints_list, detected_keypoints, personwiseKeypoints = True, None, None, None, None
threshold, nPoints, w, h =0.2, 18, 456, 256
detected_keypoints = []
while(True):
    in_rgb = q_rgb.get()
    in_pose_detections = pose_nn.tryGet()
    rgb_frame = in_rgb.getCvFrame()
    h, w = rgb_frame.shape[:2]
    if in_pose_detections is not None:
        heatmaps = np.array(in_pose_detections.getLayerFp16('Mconv7_stage2_L2')).reshape((1, 19, 32, 57)).astype('float32')
        pafs = np.array(in_pose_detections.getLayerFp16('Mconv7_stage2_L1')).reshape((1, 38, 32, 57)).astype('float32')
        new_keypoints = []
        new_keypoints_list = np.zeros((0, 3))
        keypoint_id = 0
        for row in range(18):
            probMap = heatmaps[0, row, :, :] #(32,57)
            probMap = cv2.resize(probMap, (w, h))
            mapSmooth = cv2.GaussianBlur(probMap, (3, 3), 0, 0)
            mapMask = np.uint8(mapSmooth > threshold) 
            keypoints = []
            contours, _ = cv2.findContours(mapMask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
            for cnt in contours:
                blobMask = np.zeros(mapMask.shape)
                blobMask = cv2.fillConvexPoly(blobMask, cnt, 1) 
                maskedProbMap = mapSmooth * blobMask
                _, maxVal, _, maxLoc = cv2.minMaxLoc(maskedProbMap) 
                keypoints.append(maxLoc + (probMap[maxLoc[1], maxLoc[0]],))
            new_keypoints_list = np.vstack([new_keypoints_list, *keypoints])
            keypoints_with_id = []
            for i in range(len(keypoints)):
                keypoints_with_id.append(keypoints[i] + (keypoint_id,))
                keypoint_id += 1
            new_keypoints.append(keypoints_with_id)
        valid_pairs, invalid_pairs = [], []
        n_interp_samples, paf_score_th, conf_th = 10, 0.2, 0.4

        paf_screen = np.zeros((h*2,w*2))
        shown_distance = 16
        for k in range(len(mapIdx)):  
            pafA, pafB = None, None
            pafA = pafs[0, mapIdx[k][0]-19, :, :] # shape =  (32,57)  # x
            pafB = pafs[0, mapIdx[k][1]-19, :, :] # shape =  (32,57)  # y
            pafA = cv2.resize(pafA, (w, h)) 
            pafB = cv2.resize(pafB, (w, h))
            for i in range(int(w/shown_distance)):
                for j in range(int(h/shown_distance)):
                    x_value = int(pafA[j*shown_distance][i*shown_distance]*shown_distance)
                    y_value = int(pafB[j*shown_distance][i*shown_distance]*shown_distance)
                    org_posi = (int(i*shown_distance)*2, int(j*shown_distance)*2)
                    final_posi =  (int(i*shown_distance + x_value)*2,int( j*shown_distance + y_value)*2)
                    cv2.arrowedLine(paf_screen,org_posi,final_posi,255,1,cv2.LINE_AA)
            candA = new_keypoints[POSE_PAIRS[k][0]]
            candB = new_keypoints[POSE_PAIRS[k][1]]
            if (len(candA) != 0 and len(candB) != 0):  
                valid_pair = np.zeros((0, 3))
                for i in range(len(candA)):
                    max_j, maxScore, found = -1, -1, 0
                    for j in range(len(candB)):
                        d_ij = np.subtract(candB[j][:2], candA[i][:2])
                        norm = np.linalg.norm(d_ij)
                        if norm:
                            d_ij = d_ij / norm
                        else: continue
                        interp_coord = list(zip(np.linspace(candA[i][0], candB[j][0], num=n_interp_samples), np.linspace(candA[i][1], candB[j][1], num=n_interp_samples)))
                        paf_interp = []
                        for k in range(len(interp_coord)):
                            paf_interp.append([pafA[int(round(interp_coord[k][1])), int(round(interp_coord[k][0]))], pafB[int(round(interp_coord[k][1])), int(round(interp_coord[k][0]))]])
                        paf_scores = np.dot(paf_interp, d_ij)
                        avg_paf_score = sum(paf_scores) / len(paf_scores)
                        if (len(np.where(paf_scores > paf_score_th)[0]) / n_interp_samples) > conf_th:
                            if avg_paf_score > maxScore:
                                max_j = j
                                maxScore = avg_paf_score
                                found = 1
                    if found:
                        valid_pair = np.append(valid_pair, [[candA[i][3], candB[max_j][3], maxScore]], axis=0)
                valid_pairs.append(valid_pair)
            else:
                invalid_pairs.append(k)
                valid_pairs.append([])
        keypoints_list = new_keypoints_list
        personwiseKeypoints = -1 * np.ones((0, 19))
        for k in range(len(mapIdx)):
            if k not in invalid_pairs:
                partAs, partBs = None, None
                if len(valid_pairs[k]) != 0:
                    partAs = valid_pairs[k][:, 0]
                    partBs = valid_pairs[k][:, 1]
                indexA, indexB = np.array(POSE_PAIRS[k])
                for i in range(len(valid_pairs[k])):
                    found = 0
                    person_idx = -1
                    for j in range(len(personwiseKeypoints)):
                        if personwiseKeypoints[j][indexA] == partAs[i]:
                            person_idx = j
                            found = 1
                            break
                    if found:
                        personwiseKeypoints[person_idx][indexB] = partBs[i]
                        personwiseKeypoints[person_idx][-1] += keypoints_list[partBs[i].astype(int), 2] + valid_pairs[k][i][2]
                    elif not found and k < 17:
                        row = -1 * np.ones(19)
                        row[indexA] = partAs[i]
                        row[indexB] = partBs[i]
                        row[-1] = sum(keypoints_list[valid_pairs[k][i, :2].astype(int), 2]) + valid_pairs[k][i][2]
                        personwiseKeypoints = np.vstack([personwiseKeypoints, row])
        detected_keypoints, keypoints_list, personwiseKeypoints = (new_keypoints, new_keypoints_list, personwiseKeypoints)
        for i in range(18):
            for j in range(len(detected_keypoints[i])):
                cv2.circle(rgb_frame, detected_keypoints[i][j][0:2], 5, colors[i], -1, cv2.LINE_AA)
        for i in range(17):
            for n in range(len(personwiseKeypoints)):
                index = personwiseKeypoints[n][np.array(POSE_PAIRS[i])]
                if -1 in index: continue
                B = np.int32(keypoints_list[index.astype(int), 0])
                A = np.int32(keypoints_list[index.astype(int), 1])
                cv2.line(rgb_frame, (B[0], A[0]), (B[1], A[1]), colors[i], 3, cv2.LINE_AA)
        cv2.imshow("rgb", rgb_frame)
        cv2.imshow("paf_screen", paf_screen)
    if cv2.waitKey(1) == ord('q'): break