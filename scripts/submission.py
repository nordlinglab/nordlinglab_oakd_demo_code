import depthai as dai
import numpy as np
import cv2
import matplotlib.pyplot as plt

def createDepthPipeline():
    pipeline = dai.Pipeline()
    cam_rgb = pipeline.createColorCamera()
    cam_rgb.setIspScale(1,2)
    cam_rgb.setColorOrder(dai.ColorCameraProperties.ColorOrder.BGR)
    xout_cam_rgb = pipeline.createXLinkOut()
    xout_cam_rgb.setStreamName('rgb')
    print('Created Color camera object')

    monoLeft = pipeline.createMonoCamera()
    monoRight = pipeline.createMonoCamera()
    monoLeft.setBoardSocket(dai.CameraBoardSocket.LEFT)
    monoRight.setBoardSocket(dai.CameraBoardSocket.RIGHT)
    monoLeft.setResolution(dai.MonoCameraProperties.SensorResolution.THE_400_P)
    monoRight.setResolution(dai.MonoCameraProperties.SensorResolution.THE_400_P)
    print('Created mono camera objects')

    stereo = pipeline.createStereoDepth()
    stereo.setConfidenceThreshold(200)
    stereo.setLeftRightCheck(True)
    stereo.setMedianFilter(dai.StereoDepthProperties.MedianFilter.KERNEL_7x7)
    stereo.setDepthAlign(dai.CameraBoardSocket.RGB)
    xout_depth = pipeline.createXLinkOut()
    xout_depth.setStreamName('depth')
    print('Created stereo object')

    #Linking
    cam_rgb.isp.link(xout_cam_rgb.input)
    monoLeft.out.link(stereo.left)
    monoRight.out.link(stereo.right)
    stereo.disparity.link(xout_depth.input)

    print('Created pipeline')
    return pipeline, stereo

pipeline, stereo = createDepthPipeline()
device = dai.Device(pipeline)
device.startPipeline()

q_rgb = device.getOutputQueue(name = 'rgb', maxSize = 4, blocking = False)
q_depth = device.getOutputQueue(name = 'depth', maxSize = 4, blocking = False)

distance = 0 ; n = 0
distance_pixels = []; frame_num = []
while True:
    in_rgb = q_rgb.get()
    in_depth = q_depth.get()

    rgb_frame = in_rgb.getCvFrame()
    depth_frame = in_depth.getCvFrame()

    frame_red = rgb_frame.copy()
    frame_blue = rgb_frame.copy()

    img_hsv = cv2.cvtColor(rgb_frame.copy(), cv2.COLOR_BGR2HSV)
    #Mask red objects
    lower_red = np.array([0,180,50])
    upper_red = np.array([5,255,255])
    mask_red = cv2.inRange(img_hsv,lower_red, upper_red)

    lower_red = np.array([170,150,50])
    upper_red = np.array([180,255, 255])
    mask_red += cv2.inRange(img_hsv,lower_red, upper_red)

    #Mask blue objects
    lower_blue = np.array([95, 150, 50])
    upper_blue = np.array([125,255, 255])
    mask_blue = cv2.inRange(img_hsv, lower_blue, upper_blue)

    circles_red = cv2.HoughCircles(mask_red, cv2.HOUGH_GRADIENT, 1, 2,
                               param1=70, param2=5,minRadius=6, maxRadius=11)
    circles_blue = cv2.HoughCircles(mask_blue, cv2.HOUGH_GRADIENT, 1, 2,
                                    param1=70, param2=5,minRadius=6, maxRadius=11)
    circle_threshold = 10
    pair = []
    if np.array(circles_blue).any() and np.array(circles_red).any() is not None:  # use .any to check every element
        circles_blue = circles_blue[0]
        circles_red = circles_red[0]
        for i in range(len(circles_red)):
            for j in range(len(circles_blue)):
                circle_r_error = np.abs(circles_red[i][2]-circles_blue[j][2])
                if circle_r_error < circle_threshold:  # compare r
                    pair = [i, j]
                    circle_threshold = circle_r_error
                    #print(pair, circles_red[i][2], circles_blue[j][2])
        p1, p2 = pair
        average_r = np.mean([circles_red[p1][2], circles_blue[p2][2]])
        x_red, y_red = (circles_red[p1][0], circles_red[p1][1])
        x_blue, y_blue = (circles_blue[p2][0], circles_blue[p2][1])

        bbox_red = [x_red-average_r, y_red-average_r, x_red+average_r, y_red+average_r]
        bbox_blue = [x_blue-average_r, y_blue-average_r, x_blue+average_r, y_blue+average_r]
        cv2.rectangle(rgb_frame, (int(bbox_red[0]), int(bbox_red[1])), (int(bbox_red[2]), int(bbox_red[3])), (255, 0, 0), 2)
        cv2.rectangle(rgb_frame, (int(bbox_blue[0]), int(bbox_blue[1])), (int(bbox_blue[2]), int(bbox_blue[3])), (0, 255, 0), 2)

        unit_ratio = 0.7/average_r
        #Get the depth of the ROIs
        box_blue_depth = depth_frame[int(bbox_blue[1]):int(bbox_blue[3]),int(bbox_blue[0]):int(bbox_blue[2])]
        box_red_depth = depth_frame[int(bbox_red[1]):int(bbox_red[3]),int(bbox_red[0]):int(bbox_red[2])]  #list should be [y,x]
        z_blue_disp = np.mean(box_blue_depth[np.nonzero(box_blue_depth)])//10 #cm
        z_red_disp = np.mean(box_red_depth[np.nonzero(box_red_depth)])//10 #cm
        z_blue = 3277.5/z_blue_disp
        z_red = 3277.5/z_red_disp
        if np.isnan(z_blue) == False and np.isnan(z_red) == False:
            cv2.putText(rgb_frame, str(z_blue)+' cm', (int(bbox_blue[0]), int(bbox_blue[1])), cv2.FONT_HERSHEY_TRIPLEX, 0.5, (0, 255, 0))
            cv2.putText(rgb_frame, str(z_red)+' cm', (int(bbox_red[0]), int(bbox_red[1])), cv2.FONT_HERSHEY_TRIPLEX, 0.5, (0, 255, 0))
            distance = (((x_red-x_blue)*unit_ratio)**2+((y_red-y_blue)*unit_ratio)**2+(z_red-z_blue)**2)**0.5
        print(distance)
    depth_frame = (depth_frame * (255 / stereo.getMaxDisparity())).astype(np.uint8)
    #cv2.imshow('mask_red', mask_red)
    #cv2.imshow('mask_blue', mask_blue)
    cv2.imshow('depth', depth_frame)
    cv2.imshow('test', rgb_frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

    #plot distance
    n += 1
    frame_num.append(n)
    distance_pixels.append(distance)
    plt.plot(frame_num[100:],distance_pixels[100:], 'g-')
    plt.pause(0.05)
