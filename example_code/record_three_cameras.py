import cv2
import depthai as dai
import subprocess as sp
import time

# Create pipeline
pipeline = dai.Pipeline()

# Define sources and outputs
camRgb = pipeline.createColorCamera()
camRgb.setResolution(dai.ColorCameraProperties.SensorResolution.THE_1080_P)
camRgb.setInterleaved(False)
camRgb.setColorOrder(dai.ColorCameraProperties.ColorOrder.RGB)
camRgb.setFps(120)
monoLeft = pipeline.createMonoCamera()
monoRight = pipeline.createMonoCamera()
monoLeft.setFps(120)
monoRight.setFps(120)
monoLeft.setBoardSocket(dai.CameraBoardSocket.LEFT)
monoLeft.setResolution(dai.MonoCameraProperties.SensorResolution.THE_720_P)
monoRight.setBoardSocket(dai.CameraBoardSocket.RIGHT)
monoRight.setResolution(dai.MonoCameraProperties.SensorResolution.THE_720_P)

ve1 = pipeline.createVideoEncoder()
ve2 = pipeline.createVideoEncoder()
ve3 = pipeline.createVideoEncoder()

ve1Out = pipeline.createXLinkOut()
ve2Out = pipeline.createXLinkOut()
ve3Out = pipeline.createXLinkOut()


ve1Out.setStreamName('ve1Out')
ve2Out.setStreamName('ve2Out')
ve3Out.setStreamName('ve3Out')


# Properties
# camRgb.setBoardSocket(dai.CameraBoardSocket.RGB)
# monoLeft.setBoardSocket(dai.CameraBoardSocket.LEFT)
# monoRight.setBoardSocket(dai.CameraBoardSocket.RIGHT)
# Create encoders, one for each camera, consuming the frames and encoding them using H.264 / H.265 encoding
ve1.setDefaultProfilePreset(1280, 720, 60, dai.VideoEncoderProperties.Profile.H264_MAIN)
ve2.setDefaultProfilePreset(1920, 1080, 60, dai.VideoEncoderProperties.Profile.H265_MAIN)
ve3.setDefaultProfilePreset(1280, 720, 60, dai.VideoEncoderProperties.Profile.H264_MAIN)

# Linking
monoLeft.out.link(ve1.input)
camRgb.video.link(ve2.input)
monoRight.out.link(ve3.input)
ve1.bitstream.link(ve1Out.input)
ve2.bitstream.link(ve2Out.input)
ve3.bitstream.link(ve3Out.input)


# Connect to device and start pipeline
path=r'D:\PycharmProjects\OAK_D_docs\video'
h265_path= path+'\\'+'left'+'\\' +'rgb'+ '.h265'
mp4_path=path+'\\'+'left'+'\\'+ 'rgb'+'.mp4'

mono_left_path = path + '\\' + 'left' + '\\' + 'mono_left' + '.h264'
mono_left_mp4_path = path + '\\' + 'left' + '\\' +  'mono_left' + '.mp4'

mono_right_path = path + '\\' + 'left' + '\\' + 'mono_right' + '.h264'
mono_right_mp4_path = path + '\\' + 'left' + '\\' + 'mono_right'  + '.mp4'
i =0

with dai.Device(pipeline) as dev:

    # Output queues will be used to get the encoded data from the outputs defined above
    outQ1 = dev.getOutputQueue(name='ve1Out', maxSize=60, blocking=True)
    outQ2 = dev.getOutputQueue(name='ve2Out', maxSize=60, blocking=True)
    outQ3 = dev.getOutputQueue(name='ve3Out', maxSize=60, blocking=True)
    now = time.time()
    future = now + 5
    # The .h264 / .h265 files are raw stream files (not playable yet)

    while True:
        outQ1.get().getData()
        outQ2.get().getData()
        outQ3.get().getData()
        i=i+1
        print(i)
        if i == 100:
            break


    with open(mono_left_path, 'wb') as fileMono1H264, open(h265_path, 'wb') as fileColorH265, open(mono_right_path, 'wb') as fileMono2H264:
        print("Press Ctrl+C to stop encoding...")
        while True:

            try:
                # Empty each queue
                while outQ1.has():
                    outQ1.get().getData().tofile(fileMono1H264)
                    if time.time() > future:
                        break

                while outQ2.has():
                    outQ2.get().getData().tofile(fileColorH265)
                    if time.time() > future:
                        break

                while outQ3.has():
                    outQ3.get().getData().tofile(fileMono2H264)
                    if time.time() > future:
                        break


            except KeyboardInterrupt:
                # Keyboard interrupt (Ctrl + C) detected
                break


# if want to convert h264/h265 to mp4 use ffmpge
# cmd_mono1 = 'ffmpeg -framerate 60 -i mono_left.h264 -c copy mono_left.mp4'
# cmd_rgb = 'ffmpeg -framerate 120 -i rgb.h265 -c copy rgb.mp4'
# cmd_mono2 = 'ffmpeg -framerate 60 -i mono_right.h264 -c copy mono_right.mp4'

# a = 'cd D:/PycharmProjects/OAK_D_docs/video/left'
# for i in [cmd_mono1, cmd_rgb, cmd_mono2]:
#     sp.check_call(a, shell=True)
#     sp.check_call(i, shell=True)

